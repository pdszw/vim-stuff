set nocompatible            " required
filetype off                " required

" first, need to install vundle from shell:
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
"
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
" windows vundle settings
" set rtp+=$HOME/.vim/bundle/Vundle.vim
" call vundle#begin('$HOME/.vim/bundle/')

" run ':PluginInstall' after adding anything here
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" other plugins
Plugin 'scrooloose/nerdtree'
Plugin 'altercation/vim-colors-solarized'
Plugin 'scrooloose/syntastic'
Plugin 'raimondi/delimitmate'		 " autoclosing of quotes,parens,brackets,etc
Plugin 'tpope/vim-surround'		 " for paren,brackets,quotes,tags,etc changes
Plugin 'valloric/youcompleteme'		 " code completion. many langs. requires compile
Plugin 'pangloss/vim-javascript'	 " syntax highlighting and improved indentation
Plugin 'bling/vim-airline'		 " status/tabline
Plugin 'nathanaelkane/vim-indent-guides' " highlight leading tabs/spaces 
Plugin 'nathanalderson/yang.vim'  " syntax highlighting for yang files


call vundle#end()           " required
"filetype plugin indent on   " required
" to ignore plugin indent changes, instead use:
filetype plugin on


" START syntastic recommended settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" END syntastic recommended settings

" START vim-javascript settings
let g:javascript_plugin_jsdoc = 1	" highlighting for JSDocs
let g:javascript_plugin_ngdoc = 1	" highlighting for NGDocs (reqs JSDoc plugin as well)
let g:javascript_plugin_flow = 1	" highlighting for Flow
"set foldmethod=syntax			" enables code folding based on syntax file
" END vim-javascript settings

" START solarized settings
syntax enable
set background=dark
" start gvim windows
let g:solarized_visibility = "high"
let g:solarized_termcolors = "high"
let g:solarized_visibility = 256
"  end gvim windows
"  start vim over putty
let g:solarized_bold = 0
let g:solarized_contrast = "low"
let g:solarized_termcolors = 256  " also requires `export TERM=xterm-256color` in ~/.bashrc
"  end vim over putty
colorscheme solarized
" END solarized settings

" START IndentGuides settings
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
" to make look nicer when using solarized
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#303030 ctermbg=236
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#3a3a3a ctermbg=237
" END IndentGuides settings

" START my own misc settings
set modeline

set tabstop=2
set expandtab
set shiftwidth=2
set softtabstop=2

autocmd FileType python setlocal tabstop=4 expandtab shiftwidth=4 softtabstop=4
autocmd FileType javascript setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2
autocmd FileType html setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2
autocmd FileType jsx setlocal tabstop=2	expandtab shiftwidth=2 softtabstop=2
autocmd FileType yang setlocal tabstop=2 expandtab shiftwidth=2 softtabstop=2

"set cursorcolumn
"set cursorline

set splitbelow
set splitright

" make backspace work in windows
" https://stackoverflow.com/questions/5419848/backspace-doesnt-work-in-gvim-7-2-64-bit-for-windows
" set backspace=2
" set backspace=indent,eol,start


" END my own misc settings
